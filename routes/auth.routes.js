const express = require('express');
const passport = require('passport');
const User = require('../models/User');

const router = express.Router();

router.post('/register', (req, res, next) => {
    // First try to see if it works
    // console.log('req.body', req.body);
    // return res.status(200).json('The /register endpoint is working');
    // STEP 4 (Register Strategy) - Authenticate with server
    try {
        const done = (error, savedUser) => {
            if (error) {
                return next(error);
            }
            req.logIn(user, (error) => {
                if (error) {
                    return next(error);
                }
                return res.status(201).json(savedUser);
            });
        };
        passport.authenticate('registerUser', done)(req);
    } catch (error) {
        console.log('Register error', error);
    }
});

router.post('/login', (req, res, next) => {
    try {
        const done = (error, user) => {
            if (error) {
                return next(error);
            }
            req.logIn(user, (error) => {
                if (error) {
                    return next(error);
                }
                return res.status(200).json(user);
            });
        };
        passport.authenticate('userLogin', done)(req);
    } catch (error) {
        return next(error);
    }
});

router.post('logout', (req, res, next) => {
    if (req.user) {
        // log user out
        req.logout();
        req.session.destroy(() => {
            res.clearCookie('connect.sid'); // sid stands for sessin id
            return res.status(200).json('User logged out successfully!');
        });
    } else {
        const error = new Error('No user logged');
        return next(error);
    }
});

module.exports = router;

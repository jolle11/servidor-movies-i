const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User');

const emailValidation = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const loginStrategy = new LocalStrategy(
    {
        usernameField: 'mail',
        passwordField: 'pwd',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {
        // STEP 1 - Check if user exists
        try {
            const isValidEmail = emailValidation(email);
            if (!isValidEmail) {
                const error = new Error('Email or password wrong!');
                done(error);
            }
            const existingUser = await User.findOne({ mail: email });
            if (!existingUser) {
                const error = new Error('Email or password wrong!');
                done(error);
            }
            const isValidPwd = await bcrypt.compare(password, existingUser.pwd);
            if (isValidPwd) {
                return done(null, existingUser);
            } else {
                const error = new Error('Email or password wrong!');
                return done(error);
            }
        } catch (error) {
            return done(error);
        }
    }
);

module.exports = loginStrategy;

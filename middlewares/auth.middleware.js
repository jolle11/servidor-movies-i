const isAuth = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    } else {
        return res.status(401).json('User is not logged in');
    }
};

const isAdmin = (req, res, next) => {
    if (req.isAuthenticated()) {
        // Is authenticated
        if (req.user.role === 'admin') {
            // Is admin
            return next();
        } else {
            // Is authenticated but not an admin
            return res.status(403).json("You're not admin of this service");
        }
    } else {
        // Not authenticated
        return res.status(401).json("You're not authenticated");
    }
};

module.exports = { isAuth, isAdmin };

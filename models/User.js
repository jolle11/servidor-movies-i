const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        mail: { type: String, required: true },
        pwd: { type: String, required: true },
        name: { type: String },
        role: { type: String, required: true, enum: ['user', 'admin'], default: 'user' },
    },
    { timestamps: true }
);

const User = mongoose.model('Users', userSchema);

module.exports = User;

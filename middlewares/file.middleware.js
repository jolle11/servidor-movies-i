const path = require('path');
const fs = require('fs');
const multer = require('multer');
const cloudinary = require('cloudinary').v2;

// storage is where we are going to save the files
const storage = multer.diskStorage({
    filename: (req, file, callback) => {
        callback(null, `${Date.now()}-${file.originalname}`);
    },
    destination: (req, file, callback) => {
        const directory = path.join(__dirname, '../public/uploads');
        callback(null, directory);
    },
});

const ACCEPTED_FILE_EXTENSIONS = ['image/png', 'image/jpg', 'image/jpeg'];

const fileFilter = (req, file, callback) => {
    // If file type does not match any of the ACCEPTED, we'll throw an error
    if (ACCEPTED_FILE_EXTENSIONS.includes(file.mimetype)) {
        // It's a valid mimetype
        callback(null, true);
    } else {
        // Not a valid mimetype
        const error = new Error('Invalid file type');
        error.status = 400; // Bad request
        callback(error, null);
    }
};

const upload = multer({
    storage,
    fileFilter,
});

const uploadToCloudinary = async (req, res, next) => {
    if (req.file) {
        console.log('Uploading to Cloudinary');
        const filePath = req.file.path;
        const imageFromCloudinary = await cloudinary.uploader.upload(filePath);
        console.log('Image uploaded successfully', imageFromCloudinary);
        req.avatarFromCloudinary = imageFromCloudinary.secure_url;
        await fs.unlinkSync(filePath);
        return next();
    } else {
        return next();
    }
};

module.exports = { upload, uploadToCloudinary };

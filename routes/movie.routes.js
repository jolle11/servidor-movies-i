const express = require('express');
const Movie = require('../models/Movie');
const { isAdmin } = require('../middlewares/auth.middleware');

const router = express.Router();

// POST

router.post('/create', [isAdmin], async (req, res, next) => {
    try {
        const newMovie = new Movie({
            title: req.body.title,
            director: req.body.director,
            year: req.body.year,
            genre: req.body.genre,
        });
        const createdMovie = await newMovie.save();
        return res.status(201).json(createdMovie);
    } catch (error) {
        return next(error);
    }
});

// PUT

router.put('/edit/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        const movieModifier = new Movie(req.body);
        movieModifier._id = id;
        const movieUpdated = await Movie.findByIdAndUpdate(id, movieModifier, { new: true }); // { new: true } is used to show the updated modifications in postman
        return res.status(200).json(movieUpdated);
    } catch (error) {
        return next(error);
    }
});

// DELETE

router.delete('/delete/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        await Movie.findByIdAndDelete(id);
        return res.status(200).json('The movie was deleted from the database ;)');
    } catch (error) {
        return next(error);
    }
});

// MIDDLEWARES

const ownMiddleware = (req, res, next) => {
    console.log('This is a middleware from the route');
    return next();
};

const anotherMiddleware = (req, res, next) => {
    console.log('This is another middleware -> movies.routes.js ');
    return next();
};

// GETS

router.get('/', [ownMiddleware, anotherMiddleware], async (req, res, next) => {
    // The middleware inside the route has to go as before the function  []
    try {
        console.log('req.jordi', req.jordi); // Using middleware from main index
        const movie = await Movie.find();
        return res.status(200).json(movie);
    } catch (error) {
        return next(error);
    }
});

router.get('/id/:id', async (req, res, next) => {
    try {
        const id = req.params.id;
        const movie = await Movie.findById(id);
        if (movie) {
            return res.status(200).json(movie);
        } else {
            return res.status(404).json('No movie found with this id');
        }
    } catch (error) {
        return next(error);
    }
});

router.get('/title/:title', async (req, res, next) => {
    try {
        const { title } = req.params;
        const movieByTitle = await Movie.find({ title: { $regex: new RegExp('^' + title.toLowerCase(), 'i') } });
        return res.status(200).json(movieByTitle);
    } catch (error) {
        return next(error);
    }
});

router.get('/genre/:genre', async (req, res, next) => {
    try {
        const { genre } = req.params;
        const movieByGenre = await Movie.find({ genre: { $regex: new RegExp('^' + genre.toLowerCase(), 'i') } });
        return res.status(200).json(movieByGenre);
    } catch (error) {
        return next(error);
    }
});

router.get('/year/:year', async (req, res, next) => {
    try {
        const { year } = req.params;
        const movieByYear = await Movie.find({ year: { $gt: year } });
        return res.status(200).json(movieByYear);
    } catch (error) {
        return next(error);
    }
});

module.exports = router;

const express = require('express');
const Cinema = require('../models/Cinema');
const Movie = require('../models/Movie');
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');
const { isAuth, isAdmin } = require('../middlewares/auth.middleware');

const router = express.Router();

// GET

router.get('/', [isAuth], async (req, res, next) => {
    try {
        const cinema = await Cinema.find().populate('movies');
        return res.status(200).json(cinema);
    } catch (error) {
        return next(error);
    }
});

router.get('/id/:id', async (req, res, next) => {
    try {
        const id = req.params.id;
        const cinema = await Cinema.findById(id);
        return res.status(200).json(cinema);
    } catch (error) {
        res.send('No cinema was found with this id'); // This one is working
        return next(error);
    }
});

router.get('/name/:name', async (req, res, next) => {
    try {
        const { name } = req.params;
        const cinemaByName = await Cinema.find({ name: { $regex: new RegExp('^' + name.toLowerCase(), 'i') } });
        console.log(cinemaByName);
        return res.status(200).json(cinemaByName);
    } catch (error) {
        return next(error);
    }
});

router.get('/location/:location', async (req, res, next) => {
    try {
        const { location } = req.params;
        const cinemaByLocation = await Cinema.find({ location: { $regex: new RegExp('^' + location.toLowerCase(), 'i') } });
        return res.status(200).json(cinemaByLocation);
    } catch (error) {
        return next(error);
    }
});

router.get('/movies/:movies', async (req, res, next) => {
    try {
        const { movies } = req.params;
        const cinemaByMovies = await Cinema.find({ movies: movies });
        return res.status(200).json(cinemaByMovies);
    } catch (error) {
        return next(error);
    }
});

// POST

router.post('/create', [isAdmin, upload.single('field-name'), uploadToCloudinary], async (req, res, next) => {
    try {
        const avatar = req.avatarFromCloudinary ? req.avatarFromCloudinary : null;

        const newCinema = new Cinema({
            name: req.body.name,
            location: req.body.location,
            movies: req.body.movies,
            image: avatar,
        });
        const createdCinema = await newCinema.save();
        return res.status(201).json(createdCinema);
    } catch (error) {
        return next(error);
    }
});

// PUT

router.put('/edit/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        const cinemaModifier = new Cinema(req.body);
        cinemaModifier._id = id;
        const cinemaUpdated = await Cinema.findByIdAndUpdate(
            id,
            cinemaModifier,
            {
                // $push: { movies: id } // Pushes to the array. If it exists, add the item again
                $addToSet: { movies: id }, // Method to avoid adding the item if already exists
            },
            { new: true }
        );
        return res.status(200).json(cinemaUpdated);
    } catch (error) {
        return next(error);
    }
});

// DELETE

router.delete('/delete/:id', async (req, res, next) => {
    try {
        const { id } = req.params;
        await Cinema.findByIdAndDelete(id);
        return res.status(200).json('The cinema had to close because of Covid-19');
    } catch (error) {
        return next(error);
    }
});

module.exports = router;

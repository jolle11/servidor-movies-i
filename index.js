const express = require('express');
require('dotenv').config();
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const indexRouter = require('./routes/index.routes');
const movieRouter = require('./routes/movie.routes');
const cinemaRouter = require('./routes/cinema.routes');
const authRouter = require('./routes/auth.routes');
require('./auth');
const { connectToDb, DB_URL } = require('./config/db');
const { isAdmin, isAuth } = require('./middlewares/auth.middleware');
connectToDb();

const PORT = process.env.PORT || 3000;
const server = express();
// BE CAREFUL WITH THE ORDER OF THINGS
server.use(
    session({
        secret: process.env.SESSION_SECRET, // Used to uncrypt the user session
        resave: false, // If there are changes in the session it is saved
        saveUninitialized: false, // We say false because passport will be handling our sessions
        cookie: {
            maxAge: 24 * 60 * 60 * 1000, // Max time the cookie is stored (in miliseconds). In this case one day
        },
        store: MongoStore.create({ mongoUrl: DB_URL }),
    })
);

server.use(express.json());
server.use(express.urlencoded({ extended: false }));

// Ejemplo de middleware propio
server.use((req, res, next) => {
    console.log("Hi, I'm a middleware!");
    req.jordi = 'Hi, Jordi speaking!';
    return next();
});

server.use(passport.initialize());
server.use(passport.session());

server.use('/', [isAdmin], indexRouter);
server.use('/movies', movieRouter);
server.use('/cinemas', [isAuth], cinemaRouter);
server.use('/auth', authRouter);

server.use((error, req, res, next) => {
    const status = error.status || 500;
    const message = error.message || 'Unexpected error!';

    return res.status(status).json(message);
});

const serverCallback = () => {
    console.log(`Server working at http://localhost:${PORT}`);
};

server.listen(PORT, serverCallback);

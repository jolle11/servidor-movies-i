const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const cinemaSchema = new Schema(
    {
        name: { type: String, required: true },
        location: { type: String, required: true },
        movies: [{ type: mongoose.Types.ObjectId, ref: 'Movie' }],
        image: { type: String, default: 'https://ca.res.keymedia.com/files/image/default(1).jpg' },
    },
    { timestamps: true }
);

const Cinema = mongoose.model('Cinema', cinemaSchema);
module.exports = Cinema;

const passport = require('passport');
const LocalStrategy = require('passport-local');
const User = require('../models/User');
const bcrypt = require('bcrypt');
// Next function is for email validation
const emailValidation = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};
// Next function is for password validation
const pwdValidation = (password) => {
    const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    return re.test(String(password));
};

const registerStrategy = new LocalStrategy(
    {
        usernameField: 'mail',
        passwordField: 'pwd',
        passReqToCallback: true,
    },
    // usernameField goes to the email and passwordField goes to the password
    // If passReqToCallback is false we have to delete req
    async (req, email, password, done) => {
        // Now we have to follow the steps for the register strategy
        // STEP 1 (Register Strategy) - Check if user exists
        try {
            const { name, passwordVerification } = req.body; // passwordVerification will be used to verify the password

            const isSamePassword = password === passwordVerification;

            if (!isSamePassword || !passwordVerification) {
                const error = new Error('Passwords not matching.');
                return done(error);
            }

            const isValidEmail = emailValidation(email);
            if (!isValidEmail) {
                const error = new Error('Invalid email');
                done(error);
            }
            const isValidPwd = pwdValidation(password);
            if (!isValidPwd) {
                const error = new Error('Password must contain between 6-16 characters, an upper case letter, a lower case letter, a number and an accepted symbol.');
                done(error);
            }
            const existingUser = await User.findOne({ mail: email });
            if (existingUser) {
                // .length deleted because of the findOne
                const error = new Error('User already registered.');
                error.status = 401;
                return done(error);
            }
            // STEP 2 (Register Strategy) - Encrypt the password
            const saltRounds = 11; // This will be the number ot times the password will be encrypted
            const hash = await bcrypt.hash(password, saltRounds); // This will be the ecncrypted password
            // STEP 3 (Register Strategy) - Create new user with received data
            const newUser = new User({
                mail: email,
                pwd: hash,
                name: name,
                role: 'user',
            });
            const savedUser = await newUser.save();
            return done(null, savedUser); // We use null because if not, the first argument will be the error we created above
        } catch (error) {
            return done(error, null);
        }
    }
);
// Local strategy requires two parameters, an object and a callback function

module.exports = registerStrategy;

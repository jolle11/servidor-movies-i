const mongoose = require('mongoose');
require('dotenv').config();
// const DB_URL = 'mongodb://localhost:27017/movie-server-i';
const DB_URL = process.env.DB_URL_PRODUCTION;
const CONFIG_DB = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
const connectToDb = async () => {
    try {
        const response = await mongoose.connect(DB_URL, CONFIG_DB);
        const { host, port, name } = response.connection;
        console.log(`Connecting to a ${name} in ${host}:${port}`);
    } catch (error) {
        console.log('Error connecting to the database', error);
    }
};
module.exports = {
    DB_URL,
    CONFIG_DB,
    connectToDb,
};
